import React from 'react';
import './App.css'
import Sidebar from './Components/Sidebar/Sidebar';
import About from './Components/About/About';
import Tools from './Components/Tools/Tools';
import Projects from './Components/Projects/Projects';
import WorkExp from './Components/WorkExp/WorkExp';
import Education from './Components/Education/Education';
import Contact from './Components/Contact/Contact';
import ScrollToTop from "react-scroll-to-top";
import NavbarMobileView from './Components/Sidebar/NavbarMobileView';


const App = () => {
  return(
    <div>
      <NavbarMobileView></NavbarMobileView>
      <Sidebar></Sidebar>
      <About></About>
      <WorkExp></WorkExp>
      <Tools></Tools>    
      <Education></Education>
      <Projects></Projects>
      <Contact></Contact>
      <ScrollToTop smooth
      top='400'
      color='white'
      height='20px'
      width='20px'
      style={{borderRadius:"90px",backgroundColor:"#38004c" }}></ScrollToTop>
    </div> 
  );

}

export default App;