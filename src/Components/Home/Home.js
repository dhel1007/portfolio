import React from "react";
import "./Home.css";
import Typewriter from 'typewriter-effect';
import myResume from "./Resume.pdf";
import{Link} from "react-scroll"


const Home = () => {
    return(
        <div id ="home" className="container-fluid home">

            <div className="container home-content">

                <h1>Hi I'm a </h1>
                <h3>
                <Typewriter
                    options={{
                        strings: [
                            'Electronics Engineer.', 'Full-Stack Developer.',
                            'Web Developer.'
                        ],
                        autoStart: true,
                        loop: true,
                    }}
                    />
                </h3>

                <div className="button-for-action">
                    <Link to="contact"
                            spy={true}
                            smooth={true}
                            duration={100}
                            offset={-100}
                            >
                        <div className="hire-me-button">
                        Hire Me
                        </div>
                    </Link>

                    <div className="get-resume-button">
                        <a href={myResume} download="Cancejo_resume.pdf">
                        Get Resume
                        </a>
                    </div>

                </div>

            </div>
            
        </div>
    );
};
export default Home;