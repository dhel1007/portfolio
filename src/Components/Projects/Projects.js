import React from "react";
import ProjectList from "./ProjectList";
import "./Projects.css"


const Projects = () => {

    const data = [
        {
            name: "Static Portfolio Website",
            des: "This was my very first project in bootcamp training.",
            projectlink: "https://dhel1007.github.io/webportfolio/", 
            techused: [
                {
                    techname: "HTML"
                },  
                {
                    techname: "CSS"
                }, 
                {
                    techname: "Bootstrap"
                },
                 
            ]
        },
        {
            name: "E-Commerce API",
            des: "I created this project during my bootcamp training. With a fully functional API that can handle adding, fetching, updating and archiving a product. This will incorporate to my E-Commerce Website.",
            projectlink: "https://dashboard.render.com/web/srv-cfi3ot02i3murcdi640g", 
            techused: [
                {
                    techname: "Node JS"
                }, 
                {
                    techname: "Express JS"
                }, 
                {
                    techname: "MongoDb"
                }, 
                
            ]
        },
       
        {
            name: "E-Commerce Website",
            des: "These will enable users to login to the website and add an order. Also, the admin can create, update and archive a product.",
            projectlink: "https://ecommerce-lz8k.vercel.app/ ", 
            techused: [
                {
                    techname: "MongoDb"
                }, 
                {
                    techname: "Express JS"
                }, 
                {
                    techname: "React JS"
                }, 
                {
                    techname: "Node JS"
                }, 
            ]
        },
        {
            name: "Dynamic Portfolio Website",
            des: "In this project, employer can contact me by just going to contact section. And the API will create and send me an email.",
            projectlink: "https://www.google.com/", 
            techused: [
                
                {
                    techname: "CSS"
                }, 
                {
                    techname: "Bootstrap"
                }, 
                {
                    techname: "React JS"
                },
                {
                    techname: "Node JS"
                },  
            ]
        },
        
    ]


    return(
        <div id="project" className="container mt-5 mb-3">
            <div className="section-title">
                <h5>Projects</h5>
                <span className="line"></span>
            </div>
            <div className="row">
                {data.map((item,index) => (
                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12" key={index}>
                        
                        <ProjectList {...item}/>
                    </div>
                ))}
                

            </div>
            
        
        
        </div>
    )
}

export default Projects;