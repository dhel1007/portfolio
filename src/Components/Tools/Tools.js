import React, { useState } from "react";
import "./Tools.css"

const Tools = () => {
    const data = [
        {
        name:"Full Stack Software Developer"
        },
        {
        name:"JavaScript"
        },
        {
        name:"HTML"
        },
        {
        name:"CSS"
        },
        {
        name:"Bootstrap"
        },
        {
        name:"Node JS"
        },
        {
        name:"Express JS"
        },
        {
        name:"React JS"
        },
        {
        name:"Mongodb"
        },
        {
        name:"Gitlab"
        },
        {
        name:"Github"
        },
        {
        name:"Robo 3T"
        },
        
        ]

    const colors = [
        "#6495ED",
        "#EC7063", 
        "#2980B9",
        "#DFFF00",
        "#F1C40F", 
        "#A04000",
        "#CACFD2", 
        "#5D6D7E",
        "#FF00FF", 
        "#00FFFF", 
        "#9A7D0A",
        "#F08080",
    ]
    const [showMoreTools,setshowMoreTools] = useState(6);
    const loadMore = () => {
        setshowMoreTools((prev) =>prev+3);
    }

    return(
        <div id="tools" className="container tools-section">
            <div className="section-title">
                <h5>Tools</h5>
                <span className="line"></span>

            </div>
            <div className="row">
                {data.slice(0,showMoreTools).map((item,index) =>(
                    <div className="col-xl-4 col-lg-4 col-md-6 col-sm-12" key={index}>
                        <div className={index===0?"tools-content-start tools-content":"tools-content"}>
                            <span className="tool-index"  style={{backgroundColor: colors[index]}}>
                                {index+1}
                            </span>
                            <p>{item.name}</p>
                        </div>
                                           
                    </div>
                ))}
            </div>

            {showMoreTools >= data.length ? null: (<span className="load-more-tools" onClick={loadMore}>Load More
            </span> )}

                    
        </div>

    );
}

export default Tools;