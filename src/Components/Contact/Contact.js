import React, { Fragment, useState } from "react";
import "./Contact.css"
import { RiMailSendFill } from "react-icons/ri";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Contact = () => {

    const API = "https://portfolio-api-imfz.onrender.com/sendEmail";

    const [name, setName] = useState();
    const [email, setEmail] = useState();
    const [jobtypes, setJobtypes] = useState();
    const [message, setMessage] = useState();

    const submitHandler = (e) => {
        fetch(API, {
            method: "POST",
            headers: {
                "Content-Type" : "application/json",
                Accept: "application/json",
            },
            body: JSON.stringify({
                    name,
                    email,
                    jobtypes,
                    message,
                })
            })
            .then(result => result.json())
            .then(data => {
                if(data.error){
                    toast.error(data.error, {
                        position: "top-right",
                        pauseOnHover: true,})
                }else{
                    toast.success("Your email has been sent", {
                        position: "top-right",
                        pauseOnHover: true,})
                    setName("");
                    setEmail("");
                    setJobtypes("");
                    setMessage("");
                }
            })
            .catch((error)=> console.log(error))
        
    }


    return(
        <Fragment>
        <div id="contact" className="container contact-section">

            <div className="about-title mb-3">
                    <h5>Contact me</h5> 
                    <span className="line mb-3"></span>
            </div>
            <div className="row">
                <div className="col-xl-5 col-lg-5 col-md-5 col-sm-5">
                    <div className="contact-form-image">
                        <img src="https://images.unsplash.com/photo-1661956600684-97d3a4320e45?ixlib=rb-4.0.3&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80" alt="contact form image"/>

                    </div>
                </div>
                <div className="col-xl-7 col-lg-7 col-md-7 col-sm-7">
                    <div className="contact-form-design">
                        <div className="text-center">
                            <h5>Send Me Email</h5>
                        </div>
                        <form>
                            <div className="contact-form">
                                <label className="form-label">Name</label>
                                <input type="text" className="form-control"
                                value={name}
                                onChange={(e) => setName(e.target.value)}
                                ></input>
                            </div>
                            <div className="contact-form">
                                <label className="form-label">E-mail</label>
                                <input type="email" className="form-control"
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                                ></input>
                            </div>
                            <div className="contact-form">
                                <label className="form-label">Job Types</label>
                                <select className="custom-select"
                                value={jobtypes}
                                onChange={(e) => setJobtypes(e.target.value)}
                                >
                                    <option>Full-time</option>
                                    <option>FreeLance</option>
                                    <option>Contract</option>
                                </select>
                            </div>
                            <div className="contact-form">
                                <label className="form-label">Message</label>
                               <textarea rows="4" type="text" className="form-control"
                               value={message}
                                onChange={(e) => setMessage(e.target.value)}
                               ></textarea>
                            </div>
                            <div className="btn-submit">
                                <p onClick={submitHandler}>Send Email  <RiMailSendFill/></p>
                            </div>

                        </form>

                    </div>

                </div>
               
            </div>
            <ToastContainer autoClose={1000}  />
        </div>
    </Fragment>
    )
}

export default Contact;