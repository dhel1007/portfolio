import React from "react";
import "./About.css"
import profilepic from "../../Image/windel1.jpg";

const About = () => {
    return(
        <div id="about" className="container about-section">
            <div className="about-title">
                    <h5>About me</h5> 
                    <span className="line mb-3"></span>
            </div>
            <div className="row">
                
                <div className="about-image col-12 col-md-6">
                    <img className="mr-auto" src={profilepic} alt="Profile Photo"/>
                </div>

                <div className="col-12 col-md-6 ">
                    
                    <div className="about-details px-2 pt-lg-5">

                        <p className="p-lg-5">
                            I am a registered Electronics Engineer, I have worked in telecom industry before which made me develop my leadership skill. Also, I am passionate on learning how computer softwares work. So, I enrolled and finished my Coding Bootcamp training for me to challenge myself and enhance my programming skill. Right now, I am seeking a job in which I can practice or share my skillsets. If this interest you, you can reach me out by going to my <a href="#contact">contact section</a>.
                        </p>

                    </div>

                </div>
            </div>
        
        </div>
    )
};

export default About;