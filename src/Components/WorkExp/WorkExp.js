import React from 'react';
import './WorkExp.css'
import { VerticalTimeline, VerticalTimelineElement }  from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';

import { MdWork } from "react-icons/md";

const WorkExp = () => {
    const data = [
        {
            companyName: "Fibercom Telecom Philippines",
            position: "Project Supervisor",
            des: " Implemented and delivered three projects with client satisfaction. Supervised our subcontractor to strictly follow the construction standards. Contributed my technical expertise to help ensure the credibility of a project.",
            year: "2021-2022"
          
        },
        {
            companyName: "Fiberhome",
            position: "Project Supervisor",
            des: " Implemented 15 kilometers backbone project. Supervised our subcontractor and track their daily accomplishments to meet project timeline. Reviewed and suggested changes in project design plan to hasten the implementation.",
            year: "2020-2021"
          
        },
        {
            companyName: "RGMA Network Inc.",
            position: "Broadcast Technician",
            des: " Monitor and maintained broadcast quality sent to receiver.",
            year: "2017-2020"
          
        },
        {
            companyName: "Glotech Philippines PVT Inc.",
            position: "Drive Test Engineer",
            des: "Completed and optimized our client’s network through testing and analyzing gathered data. Performs Functionality Test to enhance the existing client’s network.",
            year: "2016-2017"
          
        },
    ]

    const colors = [
        "#5D6D7E",
        "#F1C40F", 
        "#6495ED",
        "#CACFD2",
    ]

    return(
        <div id='work' className='container workexperience-section'>
              <div className="section-title">
                <h5>Work Experience</h5>
                <span className="line"></span>

            </div>
            <div className='timeline'>
                <VerticalTimeline lineColor='#FF8034'>
                    {data.map((item,index) =>(
                        <VerticalTimelineElement
                        className="vertical-timeline-element--work"
                        contentStyle={{ background: colors[index], color: '#282828' }}
                        contentArrowStyle={{ borderRight: '7px solid  rgb(33, 150, 243)' }}
                        date={item.year}
                        dateClassName = "date-class"
                        iconStyle={{ background: colors[index], color: '#fff' }}
                        icon={<MdWork />}
                        >
                        <h3 className="vertical-timeline-element-title">{item.companyName}</h3>
                        <h5 className="vertical-timeline-element-subtitle">{item.position}</h5>
                        
                        <p>
                            <h6>Duties and Accomplishments:</h6>
                            <ul>
                                <li style={{listStyleType: 'disc'}} >{item.des}</li>
                            </ul>   
                        </p>
                        </VerticalTimelineElement>
                    ))}
                    


                </VerticalTimeline>
            </div>
            
        </div>
    )
}
export default WorkExp;