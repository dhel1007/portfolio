import React, {useState} from "react";
import "./NavbarMobileView.css"
import { GiHamburgerMenu } from "react-icons/gi";
import {
    FcHome,
    FcNightPortrait,
    FcTodoList,
    FcContacts,
    FcFactory, 
} from "react-icons/fc";
import{Link} from "react-scroll"
import { MdBiotech } from "react-icons/md";
import { MdCastForEducation } from "react-icons/md";


const NavbarMobileView = () => {
    const[open, setOpen] = useState(false);

    const handleClick = () =>{
        setOpen(!open);
    }

    return(
        <div className="mobile-view-navbar">
            <div className="navbar-header">
                <p><GiHamburgerMenu size={25} onClick={handleClick}></GiHamburgerMenu></p>
            </div>

            { open ? (<div className="mobile-nav">
                <ul>
                        <li className="nav-item-mobileview">
                            <Link to="home"
                            spy={true}
                            smooth={true}
                            duration={100}
                            offset={-100}
                            ><FcHome size={25}/>Home</Link>
                        </li>

                        <li className="nav-item-mobileview">
                            <Link to="home"
                            spy={true}
                            smooth={true}
                            duration={100}
                            offset={-100}
                            >
                            <FcNightPortrait size={25}/>About</Link>
                        </li>

                        <li className="nav-item-mobileview">
                            <Link to="work"
                            spy={true}
                            smooth={true}
                            duration={100}
                            offset={-100}
                            >
                            <FcFactory size={25}/>Work Experience</Link>
                        </li>

                        <li className="nav-item-mobileview">
                            <Link to="tools"
                            spy={true}
                            smooth={true}
                            duration={100}
                            offset={-100}
                            >
                            <MdBiotech size={25} color="orange"/>Tools</Link>
                        </li>
                        

                        <li className="nav-item-mobileview">
                            <Link to="education"
                            spy={true}
                            smooth={true}
                            duration={100}
                            offset={-100}
                            >
                            <MdCastForEducation size={25}/>Education</Link>
                        </li>

                        <li className="nav-item-mobileview">
                            <Link to="project"
                            spy={true}
                            smooth={true}
                            duration={100}
                            offset={-100}
                            >
                            <FcTodoList size={25}/>Project</Link>
                        </li>

                        <li className="nav-item-mobileview">
                            <Link to="contact"
                            spy={true}
                            smooth={true}
                            duration={100}
                            offset={-100}
                            >
                            <FcContacts size={25}/>Contact</Link>
                        </li>
                    </ul>
            </div>): null}
            

        </div>
    )
}
export default NavbarMobileView;