import React from "react";
import "./SidebarList.css"
import profilepic from "../../Image/windel1.jpg";
import {
    FcHome,
    FcNightPortrait,
    FcTodoList,
    FcContacts,
    FcFactory, 
} from "react-icons/fc";
import{Link} from "react-scroll"
import { MdBiotech } from "react-icons/md";
import { MdCastForEducation } from "react-icons/md";

const SidebarList = ({expandSidebar}) =>{
    return(
        <React.Fragment>

            {expandSidebar? ( 
                <div className="navbar-items">
                    <div className="sidebar-profile-pic">

                        <img src={profilepic} alt="profile picture"/>

                    </div>
                    <ul>
                        <li className="nav-item">
                            <Link to="home"
                            spy={true}
                            smooth={true}
                            duration={100}
                            offset={-100}
                            ><FcHome size={25}/>Home</Link>
                        </li>

                        <li className="nav-item">
                            <Link to="home"
                            spy={true}
                            smooth={true}
                            duration={100}
                            offset={-100}
                            >
                            <FcNightPortrait size={25}/>About</Link>
                        </li>

                        <li className="nav-item">
                            <Link to="work"
                            spy={true}
                            smooth={true}
                            duration={100}
                            offset={-100}
                            >
                            <FcFactory size={25}/>Work Experience</Link>
                        </li>

                        <li className="nav-item">
                            <Link to="tools"
                            spy={true}
                            smooth={true}
                            duration={100}
                            offset={-100}
                            >
                            <MdBiotech size={25} color="orange"/>Tools</Link>
                        </li>
                        

                        <li className="nav-item">
                            <Link to="education"
                            spy={true}
                            smooth={true}
                            duration={100}
                            offset={-100}
                            >
                            <MdCastForEducation size={25}/>Education</Link>
                        </li>

                        <li className="nav-item">
                            <Link to="project"
                            spy={true}
                            smooth={true}
                            duration={100}
                            offset={-100}
                            >
                            <FcTodoList size={25}/>Project</Link>
                        </li>

                        <li className="nav-item">
                            <Link to="contact"
                            spy={true}
                            smooth={true}
                            duration={100}
                            offset={-100}
                            >
                            <FcContacts size={25}/>Contact</Link>
                        </li>
                    </ul>

                </div>
                ):
                (
                    <div className="navbar-items-only-icons">
                        <ul>
                        <li className="nav-item"><FcHome size={25}/></li>

                        <li className="nav-item"><FcNightPortrait size={25}/></li>

                        <li className="nav-item"><FcFactory size={25}/></li>

                        <li className="nav-item"><MdBiotech size={25} color="orange"/></li>

                        <li className="nav-item"><MdCastForEducation size={25} color="green"/></li>

                        <li className="nav-item"><FcTodoList size={25}/></li>

                        <li className="nav-item"><FcContacts size={25}/></li>
                    </ul>
                    </div>
                )
                }

           
            
        </React.Fragment>
    )
};

export default SidebarList;