import React from "react";
import './Education.css'
import { VerticalTimeline, VerticalTimelineElement }  from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';
import { FaUserGraduate } from "react-icons/fa";


const Education = () => {
    const data = [
        {
            name: "Zuitt Bootcamp Training",
            degree: "Web Developer Training",
            year: "2023 - Present"
        },
        {
            name: "Cagayan State University",
            degree: "Bachelor of Science in Electronics Engineering",
            year: "2010 - 2015"
        },
        {
            name: "Baggao National Agricultural School",
            degree: "Secondary Education",
            year: "2006 - 2010"
        },
        {
            name: "Dalla Elementary School",
            degree: "Elementary Education",
            year: "2000 - 2006"
        }
    ]
    const colors = [
        "#5D6D7E",
        "#F1C40F", 
        "#A04000",
        "#CACFD2",
    ]


    return(
        <div id="education" className="container education-section">
             <div className="section-title">
                <h5>Education</h5>
                <span className="line"></span>
            </div>
            <div className='timeline'>
                <VerticalTimeline lineColor='#FF8034'>
                    {data.map((item,index) =>(
                        <VerticalTimelineElement
                        className="vertical-timeline-element--work"
                        contentStyle={{ background: colors[index], color: '#282828' }}
                        contentArrowStyle={{ borderRight: '7px solid  rgb(255,96,63)' }}
                        date={item.year}
                        dateClassName = "date-class"
                        iconStyle={{ background: colors[index], color: '#fff' }}
                        icon={<FaUserGraduate />}
                        >
                        <h3 className="vertical-timeline-element-title">{item.name}</h3>
                        <h5 className="vertical-timeline-element-subtitle">{item.degree}</h5>
                        
                       
                        </VerticalTimelineElement>
                    ))}
                    


                </VerticalTimeline>
            </div>
            
        </div>
    )
}

export default Education;